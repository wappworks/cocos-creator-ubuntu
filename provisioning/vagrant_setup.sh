#!/usr/bin/env bash

# COCOS CREATOR PORTING REQUIREMENTS -----
# Install the appropriate Node version (10.x)
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install -y nodejs

# Install build-essential required by node-gyp
apt-get install -y build-essential

# COCOS CREATOR EXECUTION REQUIREMENTS -----
# Install libraries required for running Cocos Electron on Ubuntu
apt-get install -y xvfb \
    libgtk2.0-0 \
    libxss1 \
    libgconf-2-4 \
    libnss3