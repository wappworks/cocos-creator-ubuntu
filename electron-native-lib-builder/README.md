# Electron Native Lib Builder
______________

Electron supports native node modules. However, these native node modules have
to be specially built for the specific Electron platform. This project is set
up to build Electron native node modules which are compatible with the Cocos
Creator Electron environment.

Documentation on Electron's native node module support is documented 
[here](https://electronjs.org/docs/tutorial/using-native-node-modules).


## Requirements
- Node v10.x or higher
- NPM v6.x or higher

If the target Node version is not available through the standard Debian app 
distribution channels, it can be installed from NodeSource - the installation
instructions are defined [here](https://github.com/nodesource/distributions/blob/master/README.md#debinstall).


## Setup
Run `npm install` to set up the NodeJs environment


## Registering a native node module for Electron compilation
Run `npm install <package name>` to add a native node module to `package.json` 
for native compilation.

    
## Compiling the native node modules
On the **target platform**, run `npm start` after running `npm install`.
